package device

const (
	CmdReady       byte = 0x00
	CmdState       byte = 0x01
	CmdCardOn      byte = 0x02
	CmdCardOff     byte = 0x03
	CmdCardUnknown byte = 0x04
	CmdVolume      byte = 0x05
	CmdVolUp       byte = 0x06
	CmdVolDown     byte = 0x07
	CmdToggle      byte = 0x08

	StatePaused  byte = 0x00
	StatePlaying byte = 0x01
	StateLoading byte = 0x02
	StateError   byte = 0x03
)
