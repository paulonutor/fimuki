package device

import (
	"bufio"
	"bytes"
	"io"

	log "github.com/sirupsen/logrus"
	"github.com/tarm/serial"

	"gitlab.com/paulonutor/fimuki/internal/common"
)

type Command struct {
	Name byte
	Data []byte
}

type Device struct {
	conn   io.ReadWriteCloser
	event  chan Command
	logger log.FieldLogger
}

const (
	startMarker byte = 0xF0
	endMarker   byte = 0xF7
)

func ConnectToDevice(config common.Config, logger log.FieldLogger) (d *Device, err error) {
	logger.WithFields(log.Fields{
		"port":     config.Device.Port,
		"baudrate": config.Device.Baudrate,
	}).Debugln("Trying to connect to IO controller...")

	conn, err := serial.OpenPort(&serial.Config{
		Name: config.Device.Port,
		Baud: config.Device.Baudrate,
	})

	if err != nil {
		return
	}

	logger.Infoln("Connected to IO controller")

	d = &Device{
		conn:   conn,
		event:  make(chan Command),
		logger: logger,
	}

	go d.readSerial()

	return
}

func (d *Device) Close() error {
	d.logger.Debugln("Closing connection...")
	return d.conn.Close()
}

func (d *Device) Event() <-chan Command {
	return d.event
}

func (d *Device) Send(cmd Command) error {
	d.logger.WithFields(log.Fields{
		"command": cmd.Name,
		"data":    cmd.Data,
	}).Debugln("Sending command...")

	buffer := bytes.Buffer{}
	buffer.WriteByte(startMarker)
	buffer.WriteByte(cmd.Name)
	buffer.Write(cmd.Data)
	buffer.WriteByte(endMarker)

	_, err := buffer.WriteTo(d.conn)

	if err != nil {
		d.logger.WithError(err).Errorln("Error sending command")
	}

	return err
}

func (d *Device) readSerial() {
	reader := bufio.NewReader(d.conn)

	for {
		data, err := reader.ReadByte()

		if err != nil {
			d.logger.WithError(err).Errorln("Error reading serial data")
			return
		}

		if data == startMarker {
			cmdData, err := reader.ReadSlice(endMarker)

			if err != nil {
				d.logger.WithError(err).Errorln("Error reading command")
				continue
			}

			cmd := Command{cmdData[0], cmdData[1 : len(cmdData)-1]}

			d.logger.WithFields(log.Fields{
				"command": cmd.Name,
				"data":    cmd.Data,
			}).Debugln("Received command")

			d.event <- cmd
		}
	}
}
