package common

type Config struct {
	AmpPin int `json:"ampPin"`

	Device struct {
		Port     string `json:"port"`
		Baudrate int    `json:"baudrate"`
	} `json:"device"`

	LogLevel string `json:"logLevel"`

	MPD struct {
		Protocol string `json:"protocol"`
		Address  string `json:"address"`
		Password string `json:"password"`
	} `json:"mpd"`

	MusicDir string `json:"musicDir"`

	Server struct {
		Port int `json:"port"`
	} `json:"server"`
}
