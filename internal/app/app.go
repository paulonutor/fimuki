package app

import (
	"os/exec"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/stianeikeland/go-rpio"
	"golang.org/x/time/rate"

	"gitlab.com/paulonutor/fimuki/internal/common"
	"gitlab.com/paulonutor/fimuki/internal/device"
	"gitlab.com/paulonutor/fimuki/internal/player"
	"gitlab.com/paulonutor/fimuki/internal/store"
)

type Application struct {
	lastCard         *store.Card
	currentCard      *store.Card
	config           common.Config
	device           *device.Device
	store            *store.Store
	mpd              *player.MPD
	logger           log.FieldLogger
	volumeLimiter    *rate.Limiter
	debouncingToggle bool
	toggleChan       chan int
	quitChan         chan bool
	ampChan          chan bool
	ampPin           rpio.Pin
}

func NewApplication(config common.Config, device *device.Device, store *store.Store, mpd *player.MPD, logger log.FieldLogger) *Application {
	app := Application{
		config:        config,
		device:        device,
		store:         store,
		mpd:           mpd,
		logger:        logger,
		volumeLimiter: rate.NewLimiter(rate.Every(200*time.Millisecond), 1),
		toggleChan:    make(chan int),
		quitChan:      make(chan bool, 1),
		ampChan:       make(chan bool, 1),
		ampPin:        rpio.Pin(config.AmpPin),
	}

	rpio.Open()
	app.ampPin.Output()

	return &app
}

func (a *Application) CurrentCard() *store.Card {
	return a.currentCard
}

func (a *Application) SetCurrentCard(card *store.Card) {
	logger := a.logger.WithField("card", card)
	logger.Debugln("Setting current card...")

	if card == nil {
		if a.mpd.IsPlaying() {
			logger.Debugln("Pausing playback...")
			a.mpd.Pause()
		} else {
			a.device.Send(device.Command{device.CmdState, []byte{device.StatePaused}})
		}
	} else {
		count := a.mpd.PlaylistLength()

		if a.lastCard == nil || a.lastCard.UID != card.UID {
			logger.Debugln("Loading card playlist...")

			a.lastCard = card
			a.device.Send(device.Command{device.CmdState, []byte{device.StateLoading}})

			count = a.mpd.LoadPlaylist(card.UID)
		}

		if count > 0 {
			logger.Debugln("Starting playback...")

			a.enableAmp()
			a.mpd.Play()
		} else {
			logger.Debugln("Empty card playlist")

			select {
			case <-a.mpd.StatusChan():
			case <-time.After(100 * time.Millisecond):
			}

			a.device.Send(device.Command{Name: device.CmdCardUnknown})
		}
	}

	a.currentCard = card
}

func (a *Application) Config() common.Config {
	return a.config
}

func (a *Application) Store() *store.Store {
	return a.store
}

func (a *Application) MPD() *player.MPD {
	return a.mpd
}

func (a *Application) Run() {
	a.ampPin.Low()

	go a.ampEnabler()

	for {
		select {
		case cmd := <-a.device.Event():
			err := a.handleDeviceEvent(cmd)

			if err != nil {
				log.Fatalln(err)
			}
		case <-a.mpd.VolumeChan():
			a.device.Send(device.Command{device.CmdVolume, []byte{byte(a.mpd.Volume())}})
		case <-a.mpd.StatusChan():
			err := a.handleMPDStatus()

			if err != nil {
				log.Fatalln(err)
			}
		case <-a.quitChan:
			return
		}
	}
}

func (a *Application) Shutdown() {
	_ = a.mpd.Pause()

	a.ampPin.Low()

	a.quitChan <- true
}

func (a *Application) handleDeviceEvent(cmd device.Command) (err error) {
	switch cmd.Name {
	case device.CmdReady:
		a.device.Send(device.Command{Name: device.CmdReady})
	case device.CmdCardOn:
		uid := cmd.Data
		c, _ := a.store.GetCard(uid)

		if c == nil {
			n := store.CreateCard(uid, "")
			a.store.SaveCard(n)
			c = &n
		}

		a.SetCurrentCard(c)
	case device.CmdCardOff:
		a.SetCurrentCard(nil)
	case device.CmdVolUp:
		if a.volumeLimiter.Allow() {
			a.mpd.IncreaseVolume()
			go a.playVolumeSound()
		}
	case device.CmdVolDown:
		if a.volumeLimiter.Allow() {
			a.mpd.DecreaseVolume()
			go a.playVolumeSound()
		}
	case device.CmdToggle:
		if !a.debouncingToggle {
			a.debouncingToggle = true

			go func() {
				counter := 0
				timeout := time.After(500 * time.Millisecond)

				for {
					select {
					case <-a.toggleChan:
						counter++
					case <-timeout:
						a.debouncingToggle = false
						a.handleToggle(counter)
						return
					}
				}
			}()
		}

		a.toggleChan <- 0
	}

	return
}

func (a *Application) handleMPDStatus() error {
	if a.mpd.Error() != "" && !a.mpd.IsPlaying() {
		return a.device.Send(device.Command{device.CmdState, []byte{device.StateError}})
	}

	if a.mpd.IsPlaying() {
		a.enableAmp()
		return a.device.Send(device.Command{device.CmdState, []byte{device.StatePlaying}})
	}

	return a.device.Send(device.Command{device.CmdState, []byte{device.StatePaused}})
}

func (a *Application) handleToggle(clicks int) error {
	if !a.mpd.IsPlaying() {
		return nil
	}

	if clicks == 1 {
		return a.mpd.Next()
	}

	if a.mpd.Time() > 5 {
		return a.mpd.Seek(0)
	}

	return a.mpd.Previous()
}

func (a *Application) playVolumeSound() {
	a.logger.Debugln("Playing volume sound...")

	a.enableAmp()

	cmd := exec.Command("aplay", "volume.wav")
	err := cmd.Run()

	if err != nil {
		a.logger.WithError(err).Errorln("Could no play volume sound")
	}
}

func (a *Application) enableAmp() {
	a.ampChan <- true
}

func (a *Application) ampEnabler() {
	timeout := time.NewTimer(2 * time.Second)
	resetTimeout := func() {
		timeout.Reset(2 * time.Second)
	}

	for {
		select {
		case <-a.ampChan:
			if a.ampPin.Read() == rpio.Low {
				a.ampPin.High()
			}
			resetTimeout()
		case <-timeout.C:
			if a.mpd.IsPlaying() {
				resetTimeout()
			} else if a.ampPin.Read() == rpio.High {
				a.ampPin.Low()
			}
		case <-a.quitChan:
			return
		}
	}
}
