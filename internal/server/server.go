package server

import (
	"fmt"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/paulonutor/fimuki/internal/app"
	"gitlab.com/paulonutor/fimuki/internal/store"
)

type statusResult struct {
	Card       *store.Card `json:"card"`
	Song       *songResult `json:"song"`
	IsPlaying  bool        `json:"is_playing"`
	IsUpdating bool        `json:"is_updating"`
	Time       int         `json:"time"`
	Volume     int         `json:"volume"`
	Error      string      `json:"error,omitempty"`
}

type Server struct {
	srv    *http.Server
	app    *app.Application
	logger log.FieldLogger
}

func StartServer(app *app.Application, logger log.FieldLogger) *Server {
	port := app.Config().Server.Port
	srv := &http.Server{Addr: fmt.Sprintf(":%d", port)}

	s := &Server{srv, app, logger}

	http.Handle("/api/status", requestLogger(logger.WithField("handler", "status"))(http.HandlerFunc(s.statusHandler)))

	cardLogger := logger.WithField("handler", "card")

	ch := cardHandlers{
		cardState: app,
		store:     app.Store(),
		mpd:       app.MPD(),
	}

	http.Handle("/api/cards", requestLogger(cardLogger)(http.HandlerFunc(ch.List)))
	http.Handle("/api/cards/", requestLogger(cardLogger)(http.StripPrefix("/api/cards/", http.HandlerFunc(ch.One))))

	songLogger := logger.WithField("handler", "song")

	sh := songHandlers{
		config: app.Config(),
		store:  app.Store(),
		mpd:    app.MPD(),
	}

	http.Handle("/api/songs", requestLogger(songLogger)(http.HandlerFunc(sh.List)))
	http.Handle("/api/songs/", requestLogger(songLogger)(http.StripPrefix("/api/songs/", http.HandlerFunc(sh.One))))

	http.Handle("/", requestLogger(logger.WithField("handler", "index"))(http.HandlerFunc(s.indexHandler)))

	go func() {
		logger.WithField("port", port).Debugln("Starting server...")
		srv.ListenAndServe()
	}()

	return s
}

func (s *Server) Shutdown() {
	s.logger.Debugln("Shutting down server...")
	s.srv.Shutdown(nil)
}

func (s *Server) statusHandler(w http.ResponseWriter, r *http.Request) {
	logger := getRequestLogger(r)

	if r.Method != http.MethodGet {
		logger.Debugln("Method not allowed")
		jsonError(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	mpd := s.app.MPD()

	result := statusResult{
		Card:       s.app.CurrentCard(),
		IsPlaying:  mpd.IsPlaying(),
		IsUpdating: mpd.IsUpdating(),
		Time:       mpd.Time(),
		Volume:     mpd.Volume(),
		Error:      mpd.Error(),
	}

	curSong := mpd.CurrentSong()

	if curSong != nil {
		s := createSongResult(s.app.Store(), *curSong)
		result.Song = &s
	}

	logger.WithField("response", result).Debugln("Sending status response")
	writeJSON(w, result)
}

func (s *Server) indexHandler(w http.ResponseWriter, r *http.Request) {
	logger := getRequestLogger(r)

	path := strings.TrimPrefix(r.URL.Path, "/")
	data, err := Asset(path)

	if err != nil {
		logger.Debugln("Sending index.html")
		data, _ = Asset("index.html")
	} else {
		logger.WithField("file", path).Debugln("Sending asset")
	}

	w.Write(data)
}
