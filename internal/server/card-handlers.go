package server

import (
	"encoding/hex"
	"encoding/json"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/paulonutor/fimuki/internal/player"
	"gitlab.com/paulonutor/fimuki/internal/store"
)

type cardStater interface {
	CurrentCard() *store.Card
	SetCurrentCard(card *store.Card)
}

type cardResult struct {
	store.Card
	Playlist []player.Song `json:"playlist"`
}

type cardHandlers struct {
	cardState cardStater
	store     *store.Store
	mpd       *player.MPD
}

func (h *cardHandlers) List(w http.ResponseWriter, r *http.Request) {
	logger := getRequestLogger(r)

	if r.Method != http.MethodGet {
		logger.Debugln("Method not allowed")
		jsonError(w, "Method not allowed.", http.StatusMethodNotAllowed)
		return
	}

	logger.Debugln("Listing cards...")

	cards, err := h.store.ListCards()

	if err != nil {
		logger.WithError(err).Errorln("Error listing cards")
		jsonError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	results := make([]cardResult, len(cards))

	for i, card := range cards {
		results[i] = cardResult{
			Card:     card,
			Playlist: h.mpd.GetPlaylist(card.UID),
		}
	}

	logger.WithField("response", results).Debugln("Sending cards response")
	writeJSON(w, results)
}

func (h *cardHandlers) One(w http.ResponseWriter, r *http.Request) {
	urlParts := strings.Split(r.URL.Path, "/")
	uid := urlParts[0]

	if uid == "" {
		http.Redirect(w, r, "/api/cards", http.StatusSeeOther)
		return
	}

	logger := getRequestLogger(r).WithField("uid", uid)

	uidVal, err := hex.DecodeString(uid)

	if err != nil {
		logger.WithError(err).Debugln("Invalid id")
		jsonError(w, "Invalid id.", http.StatusBadRequest)
		return
	}

	existingCard, _ := h.store.GetCard(uidVal)

	if existingCard == nil {
		logger.Debugln("No card found with the provided id")
		jsonError(w, "No card found with the provided id.", http.StatusNotFound)
		return
	}

	if len(urlParts) > 1 {
		switch urlParts[1] {
		case "playlist":
			h.handlePlaylist(existingCard.UID, logger, w, r)
		default:
			logger.Debugln("Not found")
			jsonError(w, "Not found", http.StatusNotFound)
		}

		return
	}

	switch r.Method {
	case http.MethodGet:
		h.getOne(*existingCard, logger, w)
	case http.MethodPut:
		h.putOne(*existingCard, logger, w, r)
	case http.MethodDelete:
		h.deleteOne(uidVal, logger, w)
	default:
		logger.Debugln("Method not allowed")
		jsonError(w, "Method not allowed.", http.StatusMethodNotAllowed)
	}
}

func (h *cardHandlers) getOne(card store.Card, logger log.FieldLogger, w http.ResponseWriter) {
	logger.Debugln("Getting card info...")

	songs := h.mpd.GetPlaylist(card.UID)

	result := cardResult{
		Card:     card,
		Playlist: songs,
	}

	logger.WithField("response", result).Debugln("Sending card response")
	writeJSON(w, result)
}

func (h *cardHandlers) putOne(card store.Card, logger log.FieldLogger, w http.ResponseWriter, r *http.Request) {
	name := r.PostFormValue("name")

	if name == "" {
		logger.Debugln("Missing name parameter")
		jsonError(w, "Missing name parameter", http.StatusBadRequest)
		return
	}

	logger = logger.WithField("name", name)
	logger.Debugln("Saving card...")

	card.Name = name

	err := h.store.SaveCard(card)

	if err != nil {
		logger.WithError(err).Errorln("Error saving card")
		jsonError(w, "Error saving card", http.StatusInternalServerError)
		return
	}

	if h.cardState.CurrentCard() != nil && card.UID == h.cardState.CurrentCard().UID {
		h.cardState.SetCurrentCard(&card)
	}

	h.getOne(card, logger, w)
}

func (h *cardHandlers) deleteOne(uid []byte, logger log.FieldLogger, w http.ResponseWriter) {
	uidStr := hex.EncodeToString(uid)

	logger.Debugln("Deleting card data...")
	h.store.DeleteCard(uid)

	logger.Debugln("Deleting card playlist...")
	h.mpd.DeletePlaylist(uidStr)

	currentCard := h.cardState.CurrentCard()

	if currentCard != nil && uidStr == currentCard.UID {
		logger.Debugln("Current card deleted. Clearing card state...")
		h.cardState.SetCurrentCard(nil)
	}

	logger.Debugln("Deleted card")
	w.WriteHeader(http.StatusNoContent)
}

func (h *cardHandlers) handlePlaylist(uid string, logger log.FieldLogger, w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		h.getPlaylist(uid, logger, w)
		return
	}

	songIDs := make([]string, 0)
	err := json.NewDecoder(r.Body).Decode(&songIDs)

	if err != nil {
		logger.WithError(err).Debugln("Malformed body")
		jsonError(w, "Malformed body", http.StatusBadRequest)
		return
	}

	logger = logger.WithField("songs", songIDs)

	switch r.Method {
	case http.MethodPost:
		h.postToPlaylist(uid, songIDs, logger, w)
	case http.MethodPut:
		h.putToPlaylist(uid, songIDs, logger, w)
	case http.MethodDelete:
		h.deleteFromPlaylist(uid, songIDs, logger, w)
	default:
		jsonError(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

func (h *cardHandlers) getPlaylist(uid string, logger log.FieldLogger, w http.ResponseWriter) {
	logger.Debugln("Getting card playlist...")

	songs := h.mpd.GetPlaylist(uid)

	logger.WithField("response", songs).Debugln("Sending card playlist")
	writeJSON(w, songs)
}

func (h *cardHandlers) postToPlaylist(uid string, songIDs []string, logger log.FieldLogger, w http.ResponseWriter) {
	logger.Debugln("Adding songs to card playlist...")

	err := h.mpd.AddToPlaylist(uid, songIDs)

	if err != nil {
		logger.WithError(err).Debugln("Error adding songs to card playlist")
		jsonError(w, "Error adding songs to card playlist", http.StatusBadRequest)
		return
	}

	h.getPlaylist(uid, logger, w)
}

func (h *cardHandlers) putToPlaylist(uid string, songIDs []string, logger log.FieldLogger, w http.ResponseWriter) {
	logger.Debugln("Overwriting card playlist...")

	err := h.mpd.SavePlaylist(uid, songIDs)

	if err != nil {
		logger.WithError(err).Debugln("Error overwriting card playlist")
		jsonError(w, "Error overwriting card playlist", http.StatusBadRequest)
		return
	}

	h.getPlaylist(uid, logger, w)
}

func (h *cardHandlers) deleteFromPlaylist(uid string, songIDs []string, logger log.FieldLogger, w http.ResponseWriter) {
	logger.Debugln("Deleting songs from card playlist...")

	err := h.mpd.DeleteFromPlaylist(uid, songIDs)

	if err != nil {
		logger.WithError(err).Debugln("Error deleting songs from card playlist")
		jsonError(w, err.Error(), http.StatusBadRequest)
		return
	}

	logger.Debugln("Deleted songs from card playlist")
	w.WriteHeader(http.StatusNoContent)
}
