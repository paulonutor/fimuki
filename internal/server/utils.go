package server

import (
	"bytes"
	"context"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"image/jpeg"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/nfnt/resize"
	log "github.com/sirupsen/logrus"

	"gitlab.com/paulonutor/fimuki/internal/player"
	"gitlab.com/paulonutor/fimuki/internal/store"
)

type loggerKeyType int

type errorResult struct {
	Message string `json:"message"`
}

func trimExt(path string) string {
	return strings.TrimSuffix(path, filepath.Ext(path))
}

func isValidUID(uid string) bool {
	_, err := hex.DecodeString(uid)
	return err == nil
}

func compactValues(slice []string) []string {
	result := make([]string, 0, len(slice))

	for _, value := range slice {
		if value != "" {
			result = append(result, value)
		}
	}

	return result
}

func getArrayFormValue(values map[string][]string, name string) []string {
	if value, ok := values[name]; ok {
		return compactValues(value)
	}

	if value, ok := values[fmt.Sprintf("%s[]", name)]; ok {
		return compactValues(value)
	}

	return nil
}

func writeJSON(w http.ResponseWriter, v interface{}) (err error) {
	b, err := json.MarshalIndent(v, "", "  ")

	if err != nil {
		return
	}

	w.Header().Set("Content-Type", "application/json")

	_, err = w.Write(b)

	return
}

func jsonError(w http.ResponseWriter, msg string, code int) {
	w.WriteHeader(code)
	writeJSON(w, errorResult{msg})
}

func requestLogger(logger log.FieldLogger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctxLogger := logger.WithFields(log.Fields{
				"method": r.Method,
				"path":   r.URL.Path,
			})

			ctx := context.WithValue(r.Context(), loggerKeyType(0), ctxLogger)

			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

func getRequestLogger(r *http.Request) log.FieldLogger {
	return r.Context().Value(loggerKeyType(0)).(log.FieldLogger)
}

func createThumbnail(path string) (result []byte, err error) {
	file, err := os.Open(path)

	if err != nil {
		return
	}

	img, err := jpeg.Decode(file)

	if err != nil {
		return
	}

	thumb := resize.Thumbnail(400, 400, img, resize.Bilinear)
	buffer := &bytes.Buffer{}

	err = jpeg.Encode(buffer, thumb, nil)

	if err != nil {
		return
	}

	result = buffer.Bytes()

	return
}

func createSongResult(store *store.Store, song player.Song) songResult {
	data, _ := store.GetCoverArt(song.ID)

	return songResult{
		Song:     song,
		HasCover: data != nil,
	}
}
