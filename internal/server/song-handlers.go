package server

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/paulonutor/fimuki/internal/common"
	"gitlab.com/paulonutor/fimuki/internal/player"
	"gitlab.com/paulonutor/fimuki/internal/store"
)

type songResult struct {
	player.Song
	HasCover bool `json:"has_cover"`
}

type songHandlers struct {
	config common.Config
	store  *store.Store
	mpd    *player.MPD
}

func (h *songHandlers) List(w http.ResponseWriter, r *http.Request) {
	logger := getRequestLogger(r)

	switch r.Method {
	case http.MethodGet:
		h.listSongs(logger, w, r)
	case http.MethodPost:
		h.uploadSongs(logger, w, r)
	case http.MethodDelete:
		h.deleteSongs(logger, w, r)
	default:
		logger.Debugln("Method not allowed")
		jsonError(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

func (h *songHandlers) One(w http.ResponseWriter, r *http.Request) {
	urlParts := strings.Split(r.URL.Path, "/")
	songID := urlParts[0]

	if songID == "" {
		http.Redirect(w, r, "/api/songs", http.StatusSeeOther)
		return
	}

	logger := getRequestLogger(r).WithField("song", songID)

	foundSong, _ := h.mpd.GetSong(songID)

	if foundSong == nil {
		logger.Debugln("No song found with the provided id")
		jsonError(w, "No song found with the provided id", http.StatusNotFound)
		return
	}

	if len(urlParts) > 1 {
		switch urlParts[1] {
		case "cover":
			h.getCover(songID, logger, w, r)
		case "file":
			h.getFile(songID, logger, w, r)
		default:
			logger.Debugln("Not found")
			jsonError(w, "Not found", http.StatusNotFound)
		}

		return
	}

	switch r.Method {
	case http.MethodGet:
		result := createSongResult(h.store, *foundSong)

		logger.WithField("response", result).Debugln("Sending song response")

		writeJSON(w, result)
	case http.MethodDelete:
		logger.Debugln("Deleting song...")

		h.mpd.DeleteSong(foundSong)

		logger.Debugln("Song deleted")
		w.WriteHeader(http.StatusNoContent)
	default:
		logger.Debugln("Method not allowed")
		jsonError(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

func (h *songHandlers) listSongs(logger log.FieldLogger, w http.ResponseWriter, r *http.Request) {
	logger.Debugln("Listing all songs...")

	songs, err := h.mpd.ListSongs()

	if err != nil {
		logger.WithError(err).Errorln("Error listing songs")
		jsonError(w, "Error listing songs", http.StatusInternalServerError)
		return
	}

	result := make([]songResult, len(songs))

	for i, song := range songs {
		result[i] = createSongResult(h.store, song)
	}

	logger.WithField("response", result).Debugln("Sending songs response")
	writeJSON(w, result)
}

func (h *songHandlers) uploadSongs(logger log.FieldLogger, w http.ResponseWriter, r *http.Request) {
	var err error

	logger.Debugln("Creating temp directory...")

	tmpDir, err := ioutil.TempDir("", "fimuki-")

	if err != nil {
		logger.WithError(err).Errorln("Error creating temp directory")
		jsonError(w, "Error creating temp directory", http.StatusInternalServerError)
		return
	}

	logger = logger.WithField("tempdir", tmpDir)

	defer os.RemoveAll(tmpDir)

	url := r.PostFormValue("url")

	if url != "" {
		err = h.handleURLUpload(tmpDir, url, logger.WithField("url", url))
	} else {
		fileHeaders := r.MultipartForm.File["file[]"]

		for idx, fileHeader := range fileHeaders {
			uploadLogger := logger.WithFields(log.Fields{
				"filename": fileHeader.Filename,
				"size":     fileHeader.Size,
			})

			uploadFile, valid := h.checkFileUpload(fileHeader)

			if !valid {
				uploadLogger.Debugln("Only mp3 files are supported")
				jsonError(w, "Only mp3 files are supported", http.StatusUnsupportedMediaType)
				return
			}

			defer uploadFile.Close()

			err = h.handleFileUpload(tmpDir, idx+1, uploadFile, uploadLogger)

			if err != nil {
				break
			}
		}
	}

	if err != nil {
		jsonError(w, "Error uploading song", http.StatusInternalServerError)
		return
	}

	songs, err := h.moveUploads(tmpDir, logger)

	if err != nil {
		jsonError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	logger.WithField("response", songs).Debugln("Sending new songs response")

	w.WriteHeader(http.StatusCreated)
	writeJSON(w, songs)
}

func (h *songHandlers) deleteSongs(logger log.FieldLogger, w http.ResponseWriter, r *http.Request) {
	songIDs := make([]string, 0)
	err := json.NewDecoder(r.Body).Decode(&songIDs)

	if err != nil {
		logger.WithError(err).Debugln("Malformed body")
		jsonError(w, "Malformed body", http.StatusBadRequest)
		return
	}

	logger = logger.WithField("songs", songIDs)
	logger.Debugln("Deleting songs...")

	for _, songID := range songIDs {
		song, err := h.mpd.GetSong(songID)

		if err == nil {
			h.mpd.DeleteSong(song)
		}
	}

	logger.Debugln("Deleted songs")
	w.WriteHeader(http.StatusNoContent)
}

func (h *songHandlers) getCover(songID string, logger log.FieldLogger, w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		logger.Debugln("Method not allowed")
		jsonError(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	logger.Debugln("Getting song cover...")
	data, err := h.store.GetCoverArt(songID)

	if err != nil {
		logger.WithError(err).Errorln("Error getting song cover")
		jsonError(w, "Error getting song cover", http.StatusInternalServerError)
		return
	}

	logger.Debugln("Sending song cover")

	w.Header().Set("Content-Type", "image/jpeg")
	w.Write(data)
}

func (h *songHandlers) getFile(songID string, logger log.FieldLogger, w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		logger.Debugln("Method not allowed")
		jsonError(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	songPath := filepath.Join(h.config.MusicDir, fmt.Sprintf("%s.mp3", songID))

	logger.WithField("path", songPath).Debugln("Sending song file")
	http.ServeFile(w, r, songPath)
}

func (h *songHandlers) handleURLUpload(tmpDir string, url string, logger log.FieldLogger) error {
	cmd := exec.Command(
		"youtube-dl",
		"--format=mp3/bestaudio",
		"--extract-audio",
		"--audio-format=mp3",
		"--audio-quality=0",
		"--write-thumbnail",
		"--add-metadata",
		"--yes-playlist",
		"--output='%(playlist_index)s.%(ext)s'",
		url,
	)

	cmd.Dir = tmpDir

	logger = logger.WithField("command", strings.Join(cmd.Args, " "))
	logger.Debugln("Downloading url...")

	cmd.Stdout = log.WithField("component", "youtube-dl").WriterLevel(log.DebugLevel)
	err := cmd.Run()

	if err != nil {
		logger.WithError(err).Errorln("Error downloading url...")
	}

	return err
}

func (h *songHandlers) checkFileUpload(header *multipart.FileHeader) (io.ReadCloser, bool) {
	if !strings.HasSuffix(header.Filename, ".mp3") {
		return nil, false
	}

	uploadFile, err := header.Open()

	if err != nil {
		return nil, false
	}

	return uploadFile, true
}

func (h *songHandlers) handleFileUpload(tmpDir string, idx int, r io.Reader, logger log.FieldLogger) (err error) {
	tmpFile := fmt.Sprintf("song%d.mp3", idx)

	logger = logger.WithField("tempfile", tmpFile)
	logger.Debugln("Creating temp file...")

	file, err := os.Create(filepath.Join(tmpDir, tmpFile))

	if err != nil {
		logger.WithError(err).Errorln("Error creating temp file")
		return
	}

	logger.Debugln("Uploading data to temp file...")

	_, err = io.Copy(file, r)

	if err != nil {
		logger.WithError(err).Errorln("Error uploading data")
		return
	}

	cmd := exec.Command("ffmpeg", "-i", tmpFile, "-c:v", "copy", fmt.Sprintf("song%d.jpg", idx))
	cmd.Dir = tmpDir

	fLogger := logger.WithField("command", strings.Join(cmd.Args, " "))
	fLogger.Debugln("Extracting cover image...")

	cmd.Stdout = log.WithField("component", "ffmpeg").WriterLevel(log.DebugLevel)

	fErr := cmd.Run()

	if fErr != nil {
		fLogger.WithError(fErr).Debugln("Error extracting cover image")
	}

	return
}

func (h *songHandlers) moveUploads(tmpDir string, logger log.FieldLogger) (result []songResult, err error) {
	files, err := ioutil.ReadDir(tmpDir)

	if err != nil {
		logger.WithError(err).Errorln("Could not read temp directory")
		return
	}

	logger = logger.WithField("files", files)
	logger.Debugln("Moving songs to music directory...")

	imageMap := make(map[string]string)

	for _, file := range files {
		if filepath.Ext(file.Name()) == ".jpg" {
			imageMap[trimExt(file.Name())] = filepath.Join(tmpDir, file.Name())
		}
	}

	result = make([]songResult, 0)

	for _, file := range files {
		if filepath.Ext(file.Name()) != ".mp3" {
			continue
		}

		name := trimExt(file.Name())
		path := filepath.Join(tmpDir, file.Name())

		fileLogger := logger.WithField("file", name)
		fileLogger.Debugln("Moving file...")

		song, err := h.mpd.MoveToMusicDir(path)

		if err != nil {
			fileLogger.WithError(err).Errorln("Error moving file")
			continue
		}

		if imagePath, ok := imageMap[name]; ok {
			fileLogger.Debugln("Saving file cover...")
			data, err := createThumbnail(imagePath)

			if err == nil {
				h.store.SaveCoverArt(song.ID, data)
			} else {
				fileLogger.WithError(err).Debugln("Error saving file cover")
			}
		}

		result = append(result, createSongResult(h.store, song))
	}

	logger.WithField("count", len(result)).Debugln("Moved files to music directory")

	return
}
