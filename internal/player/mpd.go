package player

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	mpdLib "github.com/fhs/gompd/mpd"
	log "github.com/sirupsen/logrus"

	"gitlab.com/paulonutor/fimuki/internal/common"
)

type Song struct {
	ID       string `json:"id"`
	Path     string `json:"path"`
	Title    string `json:"title,omitempty"`
	Artist   string `json:"artist,omitempty"`
	Duration int    `json:"duration,omitempty"`
}

type MPD struct {
	config         common.Config
	logger         log.FieldLogger
	client         *mpdLib.Client
	watcher        *mpdLib.Watcher
	state          string
	internalSongID int
	time           int
	playlistLength int
	volume         int
	mpdVolume      int
	error          string
	updating       bool
	done           chan int
	volumeChan     chan int
	statusChan     chan int
}

var volumeSteps = [10]int{75, 77, 81, 84, 87, 89, 92, 94, 96, 98}

func ConnectToMPD(config common.Config, logger log.FieldLogger) (m *MPD, err error) {
	logger.WithFields(log.Fields{
		"protocol": config.MPD.Protocol,
		"address":  config.MPD.Address,
	}).Debugln("Trying to connect to MPD server...")

	client, err := mpdLib.DialAuthenticated(
		config.MPD.Protocol,
		config.MPD.Address,
		config.MPD.Password,
	)

	if err != nil {
		return
	}

	logger.Infoln("Connected to MPD server")
	logger.Debugln("Enabling song repeat mode...")

	err = client.Repeat(true)

	if err != nil {
		return
	}

	logger.Debugln("Starting watcher...")

	watcher, err := mpdLib.NewWatcher(
		config.MPD.Protocol,
		config.MPD.Address,
		config.MPD.Password,
	)

	if err != nil {
		return
	}

	m = &MPD{
		config:     config,
		logger:     logger,
		client:     client,
		watcher:    watcher,
		done:       make(chan int),
		volumeChan: make(chan int),
		statusChan: make(chan int),
	}

	logger.Debugln("Requesting initial status...")
	err = m.readStatus(false)

	m.adjustVolume()

	go m.pinger()

	go m.listener()

	return
}

func (m *MPD) Close() (err error) {
	m.logger.Debugln("Closing connection...")

	m.done <- 0

	err = m.client.Close()

	if err != nil {
		return
	}

	err = m.watcher.Close()

	return
}

func (m *MPD) Volume() int {
	return m.volume
}

func (m *MPD) SetVolume(volume int) error {
	if volume < 1 {
		volume = 1
	} else if volume > len(volumeSteps) {
		volume = len(volumeSteps)
	}

	m.logger.WithField("volume", volume).Debugln("Setting volume...")

	return m.client.SetVolume(volumeSteps[volume-1])
}

func (m *MPD) IncreaseVolume() error {
	return m.SetVolume(m.volume + 1)
}

func (m *MPD) DecreaseVolume() error {
	return m.SetVolume(m.volume - 1)
}

func (m *MPD) CurrentSong() *Song {
	m.logger.Debugln("Requesting current song...")

	attrs, err := m.client.CurrentSong()

	if err != nil {
		m.logger.WithError(err).Errorln("Error while receiving current song")
		return nil
	}

	m.logger.WithField("attrs", attrs).Debugln("Received current song")

	if len(attrs) == 0 {
		return nil
	}

	s := convertAttrsToSong(attrs)
	return &s
}

func (m *MPD) Time() int {
	return m.time
}

func (m *MPD) PlaylistLength() int {
	return m.playlistLength
}

func (m *MPD) IsPlaying() bool {
	return m.state == "play"
}

func (m *MPD) IsUpdating() bool {
	return m.updating
}

func (m *MPD) Error() string {
	return m.error
}

func (m *MPD) VolumeChan() <-chan int {
	return m.volumeChan
}

func (m *MPD) StatusChan() <-chan int {
	return m.statusChan
}

func (m *MPD) Play() error {
	switch m.state {
	case "pause":
		m.logger.Debugln("Unpausing...")
		return m.client.Pause(false)
	case "stop":
		if m.playlistLength > 0 {
			m.logger.Debugln("Starting playback...")
			return m.client.Play(0)
		}
	}

	return nil
}

func (m *MPD) Pause() error {
	if !m.IsPlaying() {
		return nil
	}

	m.logger.Debugln("Pausing...")

	return m.client.Pause(true)
}

func (m *MPD) Next() error {
	m.logger.Debugln("Next...")

	return m.client.Next()
}

func (m *MPD) Previous() error {
	m.logger.Debugln("Previous...")

	return m.client.Previous()
}

func (m *MPD) Seek(position int) error {
	m.logger.WithField("position", position).Debugln("Seeking...")

	return m.client.SeekID(m.internalSongID, 0)
}

func (m *MPD) ListSongs() (result []Song, err error) {
	m.logger.Debugln("Requesting song list...")

	list, err := m.client.ListAllInfo("")

	if err != nil {
		m.logger.WithError(err).Errorln("Error while receiving song list")
		return
	}

	m.logger.WithField("count", len(list)).Debugln("Received song list")

	result = make([]Song, len(list))

	for i, attrs := range list {
		result[i] = convertAttrsToSong(attrs)
	}

	return
}

func (m *MPD) GetSong(songID string) (result *Song, err error) {
	logger := m.logger.WithField("song", songID)
	logger.Debugln("Requesting song info...")

	list, err := m.client.Find("file", fmt.Sprintf("%s.mp3", songID))

	if err != nil {
		logger.WithError(err).Errorln("Error while receiving song info")
		return
	}

	if len(list) == 0 {
		return
	}

	logger.WithField("attrs", list[0]).Debugln("Received song info")

	s := convertAttrsToSong(list[0])
	result = &s

	return
}

func (m *MPD) DeleteSong(song *Song) error {
	logger := m.logger.WithField("song", song.ID)
	logger.Debugln("Deleting song...")

	fullPath := filepath.Join(m.config.MusicDir, song.Path)
	_, err := os.Stat(fullPath)

	if err != nil {
		if !os.IsNotExist(err) {
			logger.WithError(err).Errorln("Error while deleting song")
			return err
		}

		logger.Debugln("Song to delete not found on disk")
		return nil
	}

	os.Remove(fullPath)

	logger.Debugln("Waiting for database update after delete...")
	err = m.updateAndWait("")

	if err != nil {
		logger.WithError(err).Errorln("Error updating database after delete")
	}

	return err
}

func (m *MPD) MoveToMusicDir(path string) (result Song, err error) {
	songID := generateSongID()
	songPath := fmt.Sprintf("%s.mp3", songID)
	newPath := filepath.Join(m.config.MusicDir, songPath)

	logger := m.logger.WithFields(log.Fields{
		"path": path,
		"dest": newPath,
		"song": songID,
	})

	logger.Debugln("Moving song file...")
	err = os.Rename(path, newPath)

	if err != nil {
		logger.WithError(err).Errorln("Error while moving song file")
		return
	}

	logger.Debugln("Waiting for database update after move...")
	err = m.updateAndWait(songPath)

	if err != nil {
		logger.WithError(err).Errorln("Error updating database after move")
		return
	}

	s, err := m.GetSong(songID)

	if err != nil {
		return
	}

	if s != nil {
		result = *s
	}

	return
}

func (m *MPD) LoadPlaylist(name string) int {
	logger := m.logger.WithField("playlist", name)
	logger.Debugln("Clearing current playlist...")

	err := m.client.Clear()

	if err != nil {
		logger.WithError(err).Debugln("Could not clear current playlist")
		return 0
	}

	logger.Debugln("Loading new playlist...")
	err = m.client.PlaylistLoad(name, 0, -1)

	if err != nil {
		logger.WithError(err).Errorln("Could not load new playlist")
		return 0
	}

	m.readStatus(true)

	logger.Debugln("Playlist loaded")

	return m.playlistLength
}

func (m *MPD) playlistContents(name string) []mpdLib.Attrs {
	list, err := m.client.PlaylistContents(name)

	if err != nil {
		return make([]mpdLib.Attrs, 0)
	}

	return list
}

func (m *MPD) GetPlaylist(name string) (result []Song) {
	m.logger.WithField("playlist", name).Debugln("Requesting playlist info...")

	result = make([]Song, 0)
	list := m.playlistContents(name)

	m.logger.WithField("list", list).Debugln("Received playlist info")

	for _, attrs := range list {
		result = append(result, convertAttrsToSong(attrs))
	}

	return
}

func (m *MPD) AddToPlaylist(name string, songIDs []string) (err error) {
	logger := m.logger.WithFields(log.Fields{
		"playlist": name,
		"songs":    songIDs,
	})
	logger.Debugln("Adding songs to playlist...")

	list := m.playlistContents(name)

	existingMap := make(map[string]bool, len(list))

	for _, attrs := range list {
		existingMap[attrs["file"]] = true
	}

	for _, songID := range songIDs {
		songLogger := logger.WithField("song", songID)
		path := fmt.Sprintf("%s.mp3", songID)

		if existingMap[path] {
			songLogger.Debugln("Skipping song")
			continue
		}

		err = m.client.PlaylistAdd(name, path)

		if err != nil {
			songLogger.WithError(err).Errorln("Error adding songs to playlist")
			return
		}

		songLogger.Debugln("Added song")
	}

	return
}

func (m *MPD) SavePlaylist(name string, songIDs []string) error {
	m.client.PlaylistClear(name)
	return m.AddToPlaylist(name, songIDs)
}

func (m *MPD) DeleteFromPlaylist(name string, songIDs []string) (err error) {
	logger := m.logger.WithFields(log.Fields{
		"playlist": name,
		"songs":    songIDs,
	})
	logger.Debugln("Deleting songs from playlist...")

	list := m.playlistContents(name)

	removalMap := make(map[string]bool, len(songIDs))

	for _, songID := range songIDs {
		removalMap[fmt.Sprintf("%s.mp3", songID)] = true
	}

	for i := len(list) - 1; i >= 0; i-- {
		songPath := list[i]["file"]

		if removalMap[songPath] {
			logger.WithField("path", songPath).Debugln("Removing path from playlist...")
			err = m.client.PlaylistDelete(name, i)

			if err != nil {
				return
			}
		}
	}

	return
}

func (m *MPD) DeletePlaylist(name string) error {
	m.logger.WithField("name", name).Debugln("Deleting playlist...")
	return m.client.PlaylistRemove(name)
}

func (m *MPD) updateAndWait(uri string) error {
	done := make(chan bool, 1)

	// force updating state
	m.updating = true

	_, err := m.client.Update(uri)

	if err != nil {
		m.updating = false
		return err
	}

	go func() {
		for m.updating {
			time.Sleep(10 * time.Millisecond)
		}

		close(done)
	}()

	<-done

	return nil
}

func (m *MPD) pinger() {
	ticker := time.Tick(1 * time.Second)

	for {
		select {
		case <-ticker:
			m.readStatus(true)
		case <-m.done:
			return
		}
	}
}

func (m *MPD) listener() {
	events := m.watcher.Event

	for {
		select {
		case msg := <-events:
			m.logger.WithField("event", msg).Debugln("Event received")

			switch msg {
			case "mixer":
				oldVolume := m.volume
				err := m.readStatus(false)

				m.adjustVolume()

				if err == nil && oldVolume != m.volume {
					m.volumeChan <- 0
				}
			case "player", "update":
				err := m.readStatus(false)

				if err == nil {
					m.statusChan <- 0
				}
			}
		case <-m.done:
			return
		}
	}
}

func (m *MPD) readStatus(silent bool) (err error) {
	attrs, err := m.client.Status()

	if err != nil {
		m.logger.WithError(err).Errorln("Error receiving status")
		return
	}

	if !silent {
		m.logger.WithField("attrs", attrs).Debugln("Received status")
	}

	m.mpdVolume, _ = strconv.Atoi(attrs["volume"])

	_, m.updating = attrs["updating_db"]
	m.state = attrs["state"]
	m.error = attrs["error"]

	m.internalSongID, _ = strconv.Atoi(attrs["songid"])
	m.playlistLength, _ = strconv.Atoi(attrs["playlistlength"])

	fmt.Sscanf(attrs["time"], "%d", &m.time)

	return
}

func (m *MPD) adjustVolume() {
	for i := len(volumeSteps); i >= 1; i-- {
		volumeStep := volumeSteps[i-1]

		if m.mpdVolume == volumeStep {
			m.volume = i
			return
		}

		if m.mpdVolume > volumeStep {
			m.volume = i
			m.client.SetVolume(volumeStep)
			return
		}
	}

	m.volume = 1
	m.client.SetVolume(volumeSteps[0])
}

func convertAttrsToSong(attrs mpdLib.Attrs) (result Song) {
	path := attrs["file"]
	filename := filepath.Base(path)

	result.ID = strings.TrimSuffix(filename, filepath.Ext(filename))
	result.Path = path
	result.Title = attrs["Title"]
	result.Artist = attrs["Artist"]

	if duration, cErr := strconv.Atoi(attrs["Time"]); cErr == nil {
		result.Duration = duration
	}

	return
}
