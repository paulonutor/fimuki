package store

import (
	"encoding/hex"

	"github.com/boltdb/bolt"
)

type Card struct {
	UID  string `json:"uid"`
	Name string `json:"name"`
}

type Store struct {
	db *bolt.DB
}

func CreateCard(uid []byte, name string) Card {
	return Card{
		UID:  hex.EncodeToString(uid),
		Name: name,
	}
}

func OpenStore() (s *Store, err error) {
	db, err := bolt.Open("data.db", 0600, nil)

	if err != nil {
		return
	}

	db.Update(func(tx *bolt.Tx) (err error) {
		_, err = tx.CreateBucketIfNotExists([]byte("cards"))

		if err != nil {
			return
		}

		_, err = tx.CreateBucketIfNotExists([]byte("covers"))

		return
	})

	s = &Store{db}

	return
}

func (s *Store) Close() error {
	return s.db.Close()
}

func (s *Store) ListCards() (result []Card, err error) {
	result = make([]Card, 0)

	err = s.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("cards"))

		return b.ForEach(func(k, v []byte) error {
			result = append(result, CreateCard(k, string(v)))
			return nil
		})
	})

	return
}

func (s *Store) GetCard(uid []byte) (result *Card, err error) {
	err = s.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("cards"))
		v := b.Get(uid)

		if v != nil {
			c := CreateCard(uid, string(v))
			result = &c
		}

		return nil
	})

	return
}

func (s *Store) SaveCard(c Card) error {
	return s.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("cards"))
		uid, err := hex.DecodeString(c.UID)

		if err != nil {
			return err
		}

		return b.Put(uid, []byte(c.Name))
	})
}

func (s *Store) DeleteCard(uid []byte) (err error) {
	return s.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("cards"))
		return b.Delete(uid)
	})
}

func (s *Store) GetCoverArt(songID string) (result []byte, err error) {
	err = s.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("covers"))
		result = b.Get([]byte(songID))
		return nil
	})

	return
}

func (s *Store) SaveCoverArt(songID string, data []byte) error {
	return s.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("covers"))
		return b.Put([]byte(songID), data)
	})
}

func (s *Store) DeleteCoverArt(songID string) (err error) {
	return s.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("covers"))
		return b.Delete([]byte(songID))
	})
}
