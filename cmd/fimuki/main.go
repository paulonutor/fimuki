package main

import (
	"encoding/json"
	"os"
	"os/signal"
	"syscall"

	log "github.com/sirupsen/logrus"

	"gitlab.com/paulonutor/fimuki/internal/app"
	"gitlab.com/paulonutor/fimuki/internal/common"
	"gitlab.com/paulonutor/fimuki/internal/device"
	"gitlab.com/paulonutor/fimuki/internal/player"
	"gitlab.com/paulonutor/fimuki/internal/server"
	"gitlab.com/paulonutor/fimuki/internal/store"
)

func loadConfig() (c common.Config, err error) {
	file, err := os.Open("config.json")

	if err != nil {
		return
	}

	defer file.Close()

	err = json.NewDecoder(file).Decode(&c)

	return
}

func main() {
	log.SetOutput(os.Stdout)
	// set default log level
	log.SetLevel(log.InfoLevel)

	config, err := loadConfig()

	if err != nil {
		log.WithError(err).Fatalln("Error loading configuration")
		return
	}

	logLevel, err := log.ParseLevel(config.LogLevel)

	if err == nil {
		log.SetLevel(logLevel)
	}

	store, err := store.OpenStore()

	if err != nil {
		log.WithError(err).Fatalln("Error opening datastore")
		return
	}

	defer store.Close()

	deviceLogger := log.WithField("prefix", "device")
	device, err := device.ConnectToDevice(config, deviceLogger)

	if err != nil {
		log.WithError(err).Fatalln("Error connecting to IO controller")
		return
	}

	defer device.Close()

	mpdLogger := log.WithField("prefix", "mpd")
	mpd, err := player.ConnectToMPD(config, mpdLogger)

	if err != nil {
		log.WithError(err).Fatalln("Error connecting to MPD")
		return
	}

	defer mpd.Close()

	appLogger := log.WithField("prefix", "app")
	app := app.NewApplication(config, device, store, mpd, appLogger)

	srvLogger := log.WithField("prefix", "server")
	srv := server.StartServer(app, srvLogger)

	defer srv.Shutdown()

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt, syscall.SIGTERM)

	go func() {
		<-signals
		app.Shutdown()
	}()

	app.Run()
}
