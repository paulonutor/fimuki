BINARY := fimuki
WEB_DIR := web
WEB_DIST_DIR := $(WEB_DIR)/dist
BUILD_DEST := bin

all: clean bindata build

clean:
	-rm -r $(BUILD_DEST)

web:
	cd $(WEB_DIR) && \
	npm install && \
	npm run build -s

bindata: web
	go-bindata -o internal/server/bindata.go -pkg server -prefix "$(WEB_DIST_DIR)" $(WEB_DIST_DIR)

$(BUILD_DEST):
	mkdir -p $@

build: \
	$(BINARY)-linux-amd64 \
	$(BINARY)-linux-arm6 \
	$(BINARY)-darwin-amd64

$(BINARY)-linux-%: GOOS = linux
$(BINARY)-darwin-%: GOOS = darwin

$(BINARY)-%-amd64: GOARCH = amd64
$(BINARY)-%-arm6: GOARCH = arm
$(BINARY)-%-arm6: GOARM=6

$(BINARY)-%: $(BUILD_DEST)
	GOOS=$(GOOS) GOARCH=$(GOARCH) GOARM=$(GOARM) go build -o $(BUILD_DEST)/$(BINARY)-$(GOOS)-$(GOARCH)$(GOARM) cmd/$(BINARY)/*.go

.PHONY: clean web bindata build
