#ifndef BUTTON_H
#define BUTTON_H

#include <Arduino.h>

#include <Bounce2.h>

#define LONG_PRESS_DELAY 1000
#define LONG_PRESS_INTERVAL 200
#define DEBOUNCER_INTERVAL 10

typedef void (*TriggerCallback)();

class Button
{
  public:
    Button(byte pin);
    void begin();
    void update();
    void onTrigger(TriggerCallback callback);

  private:
    Bounce debouncer;

    TriggerCallback callback;

    byte pin;
    bool pressed;
    bool longPress;
    unsigned long pressTime;
    unsigned long lastTrigger;
};

#endif
