#ifndef RING_H
#define RING_H

#include <FastLED.h>

#define RING_DATA_PIN 6

enum RingState
{
    BOOTING,
    IDLE,
    WAITING,
    UNKNOWN,
    LOADING,
    RUNNING,
    ERROR
};

class Ring
{
  public:
    Ring(int numLeds);
    void begin();
    void update();
    void setState(RingState state);
    void setVolume(byte volume);

  private:
    CRGB *leds;
    int numLeds;

    RingState state;
    unsigned long stateStart;

    bool showVolume;
    unsigned long volumeStart;
};

#endif
