#include "Ring.h"

unsigned char breath(unsigned long time, int seconds)
{
    return (unsigned char)((exp(sin((2 * PI * (time / (seconds * 1000.0))) - 1.57079632)) - 0.36787944) * 108.0);
}

Ring::Ring(int numLeds) : leds(new CRGB(numLeds)),
                          numLeds(numLeds),
                          state(RingState::BOOTING),
                          showVolume(false)
{
    FastLED.addLeds<NEOPIXEL, RING_DATA_PIN>(leds, numLeds);

    FastLED.setMaxRefreshRate(0);
    FastLED.setCorrection(LEDColorCorrection::TypicalPixelString);
    //FastLED.setBrightness(16);
    //FastLED.setDither(0);
}

void Ring::begin()
{
    // reset leds to black
    FastLED.clear(true);
}

void Ring::update()
{
    bool show = false;
    unsigned long stateTime = millis() - stateStart;

    EVERY_N_MILLIS(8)
    {
        if (showVolume && millis() - volumeStart > 2500)
        {
            // reset leds to black
            FastLED.clear(true);

            showVolume = false;
        }

        if (state != RingState::IDLE && !showVolume)
        {
            switch (state)
            {
            case RingState::BOOTING:
                fill_solid(leds, numLeds, CRGB(CRGB::White).nscale8(breath(stateTime, 5)));
                break;
            case RingState::WAITING:
                if (stateTime >= 30000)
                {
                    setState(RingState::IDLE);
                }
                else
                {
                    fill_solid(leds, numLeds, CRGB(CRGB::Magenta).nscale8(breath(stateTime, 5)));
                }
                break;
            case RingState::UNKNOWN:
                fill_solid(leds, numLeds, CRGB(CRGB::Blue).nscale8(breath(stateTime, 3)));
                break;
            case RingState::LOADING:
                fill_solid(leds, numLeds, CRGB(CRGB::Green).nscale8(breath(stateTime, 3)));
                break;
            case RingState::RUNNING:
                fill_rainbow(leds, numLeds, (255 / 10000.0) * (stateTime % 10000));
                break;
            case RingState::ERROR:
                if (stateTime >= 10000)
                {
                    setState(RingState::IDLE);
                }
                else
                {
                    fill_solid(leds, numLeds, CRGB(CRGB::Red).nscale8(breath(stateTime, 2)));
                }
                break;
            default:
                break;
            }

            show = true;
        }

        if (show)
        {
            FastLED.show();
        }
    }
}

void Ring::setState(RingState state)
{
    if (this->state != state)
    {
        this->state = state;
        stateStart = millis();

        // reset leds to black
        FastLED.clear(true);
    }
}

void Ring::setVolume(unsigned char volume)
{
    showVolume = true;
    volumeStart = millis();

    FastLED.clear();

    fill_solid(leds, numLeds, CRGB(CRGB::Yellow).nscale8(255 / 100.0 * volume));
    FastLED.show();
}
