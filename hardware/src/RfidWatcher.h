#ifndef RFIDWATCHER_H
#define RFIDWATCHER_H

#include <SPI.h>
#include <MFRC522.h>

#define RFID_UPDATE_INTERVAL 1000

typedef struct
{
    byte size;
    byte *value;
} CardUid;

typedef void (*CardOnCallback)(CardUid *);
typedef void (*CardOffCallback)();

class RfidWatcher
{
  public:
    RfidWatcher(byte chipSelectPin, byte resetPin);
    void begin();
    void update();
    void onCardOn(CardOnCallback callback);
    void onCardOff(CardOffCallback callback);
    CardUid *getUid();
    bool isCardPresent();

  private:
    bool differentUid();

    CardOnCallback cardOnCallback;
    CardOffCallback cardOffCallback;

    MFRC522 rfid;
    MFRC522::Uid curUid;

    unsigned long lastUpdate;
    bool cardPresent;
};

#endif
