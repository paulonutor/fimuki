#ifndef COMMANDS_H
#define COMMANDS_H

const int START_MARKER = 0xF0;
const int END_MARKER = 0xF7;

const int CMD_READY = 0x00;
const int CMD_STATE = 0x01;
const int CMD_CARD_ON = 0x02;
const int CMD_CARD_OFF = 0x03;
const int CMD_CARD_UNKNOWN = 0x04;
const int CMD_VOLUME = 0x05;
const int CMD_VOL_UP = 0x06;
const int CMD_VOL_DOWN = 0x07;
const int CMD_TOGGLE = 0x08;

const int STATE_PAUSED = 0x00;
const int STATE_PLAYING = 0x01;
const int STATE_LOADING = 0x02;
const int STATE_ERROR = 0x03;

#endif
