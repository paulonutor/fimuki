#include "RfidWatcher.h"

RfidWatcher::RfidWatcher(byte chipSelectPin, byte resetPin) : rfid(chipSelectPin, resetPin),
                                                              lastUpdate(0),
                                                              cardPresent(false)
{
}

void RfidWatcher::begin()
{
    SPI.begin();

    rfid.PCD_Init();
    rfid.PCD_SetAntennaGain(rfid.RxGain_max);
}

void RfidWatcher::update()
{
    if (millis() - lastUpdate >= RFID_UPDATE_INTERVAL)
    {
        byte buffer[2];
        byte bufferSize = sizeof(buffer);

        // Set any new card to READY
        MFRC522::StatusCode result = rfid.PICC_WakeupA(buffer, &bufferSize);

        if (result == MFRC522::StatusCode::STATUS_OK ||
            result == MFRC522::StatusCode::STATUS_COLLISION)
        {
            if (rfid.PICC_ReadCardSerial() && (!cardPresent || differentUid()))
            {
                cardPresent = true;
                curUid = rfid.uid;
                cardOnCallback(getUid());
            }

            rfid.PICC_HaltA();
        }
        else if (cardPresent)
        {
            cardPresent = false;
            cardOffCallback();
        }

        lastUpdate = millis();
    }
}

void RfidWatcher::onCardOn(CardOnCallback callback)
{
    cardOnCallback = callback;
}

void RfidWatcher::onCardOff(CardOffCallback callback)
{
    cardOffCallback = callback;
}

bool RfidWatcher::differentUid()
{
    return curUid.size != rfid.uid.size ||
           memcmp(curUid.uidByte, rfid.uid.uidByte, curUid.size) != 0;
}

CardUid *RfidWatcher::getUid()
{
    CardUid *result = new CardUid;
    result->size = curUid.size;
    result->value = curUid.uidByte;

    return result;
}

bool RfidWatcher::isCardPresent()
{
    return cardPresent;
}
