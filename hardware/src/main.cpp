#include <Arduino.h>

#include "RfidWatcher.h"
#include "Ring.h"
#include "Controls.h"
#include "commands.h"

#define RFID_RESET_PIN 9
#define RFID_CS_PIN 10

#define RING_LED_COUNT 1

#define ENCODER_A_PIN 3
#define ENCODER_B_PIN 2
#define ENCODER_BTN_PIN 4

#define MAX_CMD_BYTES 64

#define INIT_CHECK_INTERVAL 1000

byte cmdSize;
byte cmdBuffer[MAX_CMD_BYTES];
bool parsingCmd = false;

unsigned long lastInitCheck = 0;
bool initialized = false;

RfidWatcher watcher(RFID_CS_PIN, RFID_RESET_PIN);

Ring ring(RING_LED_COUNT);

Controls controls(ENCODER_A_PIN, ENCODER_B_PIN, ENCODER_BTN_PIN);

void sendCommand(byte cmd, byte size, byte *data)
{
    Serial.write(START_MARKER);
    Serial.write(cmd);

    for (int i = 0; i < size; i++)
    {
        Serial.write(data[i]);
    }

    Serial.write(END_MARKER);
}

void sendCommand(byte cmd)
{
    sendCommand(cmd, 0, NULL);
}

void handleCommand()
{
    if (cmdSize == 0)
    {
        return;
    }

    byte cmd = cmdBuffer[0];

    switch (cmd)
    {
    case CMD_READY:
        initialized = true;
        ring.setState(RingState::WAITING);
        break;
    case CMD_STATE:
        if (cmdSize < 2)
        {
            return;
        }

        switch (cmdBuffer[1])
        {
        case STATE_PAUSED:
            ring.setState(RingState::WAITING);
            break;
        case STATE_PLAYING:
            ring.setState(RingState::RUNNING);
            break;
        case STATE_LOADING:
            ring.setState(RingState::LOADING);
            break;
        case STATE_ERROR:
            ring.setState(RingState::ERROR);
            break;
        }

        break;
    case CMD_CARD_UNKNOWN:
        ring.setState(RingState::UNKNOWN);
        break;
    case CMD_VOLUME:
        if (cmdSize < 2)
        {
            return;
        }

        byte volume = cmdBuffer[1];
        ring.setVolume(volume > 100 ? 100 : volume);

        break;
    }
}

void readSerial()
{
    while (Serial.available())
    {
        int data = Serial.read();

        if (data < 0)
        {
            return;
        }

        if (parsingCmd)
        {
            if (data == END_MARKER)
            {
                parsingCmd = false;
                handleCommand();
            }
            else
            {
                cmdBuffer[cmdSize++] = data;
            }
        }
        else if (data == START_MARKER)
        {
            parsingCmd = true;
            cmdSize = 0;
        }
    }
}

void checkInit()
{
    if (!initialized && millis() - lastInitCheck >= INIT_CHECK_INTERVAL)
    {
        sendCommand(CMD_READY);
        lastInitCheck = millis();
    }
}

void setup()
{
    Serial.begin(57600);

    watcher.onCardOn([](CardUid *uid) {
        sendCommand(CMD_CARD_ON, uid->size, uid->value);
    });

    watcher.onCardOff([]() {
        sendCommand(CMD_CARD_OFF);
    });

    watcher.begin();

    ring.begin();

    controls.onLeft([]() {
        sendCommand(CMD_VOL_DOWN);
    });

    controls.onRight([]() {
        sendCommand(CMD_VOL_UP);
    });

    controls.onClick([]() {
        sendCommand(CMD_TOGGLE);
    });

    controls.begin();

    sendCommand(CMD_READY);
}

void loop()
{
    readSerial();
    checkInit();

    ring.update();

    if (!initialized)
    {
        return;
    }

    watcher.update();

    controls.update();
}
