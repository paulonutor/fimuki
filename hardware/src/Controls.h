#ifndef CONTROLS_H
#define CONTROLS_H

#include <Arduino.h>

#include <Bounce2.h>
#include <Encoder.h>

#define DEBOUNCER_INTERVAL 10

typedef void (*TriggerCallback)();

class Controls
{
  public:
    Controls(int aPin, int bPin, int btnPin);
    void begin();
    void onLeft(TriggerCallback callback);
    void onRight(TriggerCallback callback);
    void onClick(TriggerCallback callback);
    void update();

  private:
    Bounce debouncer;
    Encoder encoder;

    int btnPin;
    int lastPosition;

    TriggerCallback leftCallback;
    TriggerCallback rightCallback;
    TriggerCallback buttonCallback;
};

#endif
