#include "Controls.h"

Controls::Controls(int aPin, int bPin, int btnPin) : encoder(aPin, bPin),
                                                     btnPin(btnPin),
                                                     lastPosition(0)
{
}

void Controls::begin()
{
    debouncer.attach(btnPin, INPUT_PULLUP);
    debouncer.interval(DEBOUNCER_INTERVAL);
}

void Controls::onLeft(TriggerCallback callback)
{
    leftCallback = callback;
}

void Controls::onRight(TriggerCallback callback)
{
    rightCallback = callback;
}

void Controls::onClick(TriggerCallback callback)
{
    buttonCallback = callback;
}

void Controls::update()
{
    int position = encoder.read() / 4;

    if (position != 0)
    {
        if (position < 0)
        {
            leftCallback();
        }
        else
        {
            rightCallback();
        }

        encoder.write(0);
    }

    bool changed = debouncer.update();

    if (changed)
    {
        int value = debouncer.read();

        if (value == LOW)
        {
            buttonCallback();
        }
    }
}
