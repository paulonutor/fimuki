#include "Button.h"

Button::Button(byte pin) : pin(pin),
                           pressed(false),
                           longPress(false),
                           pressTime(0),
                           lastTrigger(0)
{
}

void Button::begin()
{
    debouncer.attach(pin, INPUT_PULLUP);
    debouncer.interval(DEBOUNCER_INTERVAL);
}

void Button::update()
{
    bool changed = debouncer.update();

    if (changed)
    {
        int value = debouncer.read();

        if (value == LOW)
        {
            pressed = true;

            callback();
            pressTime = millis();
        }
        else
        {
            pressed = false;
            longPress = false;
        }
    }
    else if (pressed)
    {
        if ((!longPress && millis() - pressTime >= LONG_PRESS_DELAY) ||
            (longPress && millis() - lastTrigger >= LONG_PRESS_INTERVAL))
        {
            longPress = true;

            callback();
            lastTrigger = millis();
        }
    }
}

void Button::onTrigger(TriggerCallback callback)
{
    this->callback = callback;
}
