export { default as deepEqual } from "deep-equal";

export function pad(val, count = 2, filler = "0") {
    return (filler.repeat(count) + val).substr(-count);
}

export function formatDuration(seconds, includeHours = true) {
    const hours = parseInt(seconds / 3600);
    const minutes = parseInt((seconds % 3600) / 60);
    const parts = [pad(minutes), pad(seconds % 60)];

    if (includeHours || hours > 0) {
        parts.unshift(pad(hours));
    }

    return parts.join(":");
}

export function debounce(func, wait) {
    let timeout;

    return function(...args) {
        const context = this;

        clearTimeout(timeout);

        timeout = setTimeout(function() {
            timeout = null;
            func.apply(context, args);
        }, wait);
    };
}
