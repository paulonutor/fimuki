import Vue from "vue";
import VueRouter from "vue-router";
import Buefy from "buefy";
import "buefy/lib/buefy.css";

import { formatDuration } from "./utils";
import App from "./App";
import StartPage from "@/components/StartPage";
import CardsPage from "@/components/CardsPage";
import CardPage from "@/components/CardPage";
import SongsPage from "@/components/SongsPage";

Vue.config.devtools = false;
Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(Buefy);

Vue.filter("duration", function(value) {
    if (Number.isNaN(value)) {
        return "";
    }

    return formatDuration.apply(null, arguments);
});

const router = new VueRouter({
    mode: "history",
    routes: [
        { path: "/", component: StartPage },
        { path: "/cards", name: "cards", component: CardsPage },
        { path: "/cards/:cardId", name: "card", component: CardPage, props: true },
        { path: "/songs", component: SongsPage },
    ],
});

new Vue({
    router,
    el: "#app",
    render: h => h(App),
});
